﻿Public Class Form1

    Private informe As Informe = New Informe()
    Private generadorHtml As GeneradorHtml = New GeneradorHtml()
    Private email As Email = New Email()

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Logger.crearArchivoLog()
            Hora.actualizaHoras()
            If (Hora.horaReal >= Hora.horaInicial And Hora.horaReal <= Hora.horaFinal) Then
                Dim registroas As List(Of Registro) = informe.getListaRegistros()
                Dim mensaje = generadorHtml.generarHtml(registroas)
                email.enviarCorreoInforme(mensaje)
            End If
        Catch ex As Exception
            Logger.escribeLog("Error al crear informe por hora" & vbCrLf & ex.ToString)
            email.enviarCorreoError(Logger.Pathtxt, "Exception")
        End Try
        Me.Close()
    End Sub
End Class
