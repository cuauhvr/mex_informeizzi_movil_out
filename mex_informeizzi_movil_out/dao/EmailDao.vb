﻿Imports System.Data.SqlClient

Public Class EmailDao
    Private Const strIntra2 As String = "Data Source=10.5.73.241;Initial Catalog=INTRANET_AXTEL;Integrated Security=False;User ID=APIUser;Password=B12.2021@;Connect Timeout=50000;"
    Private conex2 As New SqlConnection(strIntra2)

    Function getCuentas(ByVal tipo As String) As List(Of String)
        Dim command As New SqlCommand()
        Dim resultado As SqlDataReader
        Dim cuenta As String
        Dim lscuentas As New List(Of String)
        Try
            command.Connection = conex2
            command.CommandText = "dbo.SP_getMailInformes"
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.AddWithValue("@proyecto", "IZZIMOVILOUT")
            command.Parameters.AddWithValue("@tipo", tipo)
            command.CommandType = CommandType.StoredProcedure
            command.Connection.Open()
            resultado = command.ExecuteReader()
            command.CommandTimeout = Integer.MaxValue

            While resultado.Read()
                cuenta = resultado("cuenta")
                lscuentas.Add(cuenta)
            End While

            command.Connection.Close()
        Catch ex As Exception
            command.Connection.Close()
            Logger.escribeLog("Error en getMails " & vbCrLf & ex.ToString)
            Throw ex
        End Try
        Return lscuentas
    End Function

End Class
