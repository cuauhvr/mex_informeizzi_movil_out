﻿Imports System.Data.SqlClient
Imports MySql.Data.MySqlClient

Public Class InformeDao
    Dim strIntra As String = "Data Source=10.5.73.241;Initial Catalog=INTRANET_IZZI;Integrated Security=False;User ID=APIUser;Password=B12.2021@;Connect Timeout=50000;"
    Private conex As New SqlConnection(strIntra)

    Dim strDgdtlDB As String = "Data Source=85.62.101.36;Database=crm;User Id=usrclaro;Password=telestant@2014;default command timeout=50000; Allow Zero Datetime=True;Convert Zero Datetime=True;"
    Dim oCnDgtlDB As MySqlConnection = New MySqlConnection(strDgdtlDB)

    Dim strIncNegocio As String = "Data Source=10.150.78.13;Initial Catalog=TelestantIZZI;Integrated Security=False;User ID=gestion_telemark;Password=hN9rWqgntbwiu7Cvag;Connect Timeout=50000;"
    Private conexIncNegocio As New SqlConnection(strIncNegocio)

    Public Function getDatosIzziMovilOut(tipo As String, hr As Integer) As Integer
        Dim command As New SqlCommand()
        Dim registro As Integer = 0
        Dim fecha As Date = IIf(Hora.horaMexico.Hour = 0,
                                Hora.horaMexico.AddDays(-1).ToShortDateString,
                                Hora.horaMexico.ToShortDateString)
        Try
            command.Connection = conexIncNegocio
            command.CommandText = "dbo.SP_GetInformeData"
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.AddWithValue("@fecha", fecha.ToString("yyyy-MM-dd"))
            command.Parameters.AddWithValue("@hora", hr)
            command.Parameters.AddWithValue("@tipo", tipo)
            command.CommandType = CommandType.StoredProcedure
            command.Connection.Open()
            Dim resultado As SqlDataReader = command.ExecuteReader()
            command.CommandTimeout = Integer.MaxValue

            While resultado.Read()
                registro = resultado("Nro")
                Exit While
            End While
            command.Connection.Close()
        Catch ex As Exception
            command.Connection.Close()
            Logger.escribeLog("Error al consultar datos del tipo: " & tipo & vbCrLf & ex.ToString)
            Throw ex
        End Try
        Return registro
    End Function

    Public Function getVentas(ByVal hr As String) As Integer
        Dim command As New SqlCommand()
        Dim registro As Integer = 0
        Dim fecha As Date = IIf(Hora.horaMexico.Hour = 0,
                                Hora.horaMexico.AddDays(-1).ToShortDateString,
                                Hora.horaMexico.ToShortDateString)
        Try
            command.Connection = conex
            command.CommandText = "dbo.SP_GetVentasInf"
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.AddWithValue("@fecha", fecha.ToString("yyyy-MM-dd"))
            command.Parameters.AddWithValue("@hora", hr)
            command.Parameters.AddWithValue("@tipo", "IZZI_MOVIL_OUT")
            command.CommandType = CommandType.StoredProcedure
            command.Connection.Open()
            Dim resultado As SqlDataReader = command.ExecuteReader()
            command.CommandTimeout = Integer.MaxValue

            While resultado.Read()
                registro = resultado("Nro")
                Exit While
            End While
            command.Connection.Close()
        Catch ex As Exception
            command.Connection.Close()
            Logger.escribeLog("Error en CalculoVentas por producto " & hr & ": " & vbCrLf & ex.ToString)
            Throw ex
        End Try
        Return registro
    End Function

End Class
