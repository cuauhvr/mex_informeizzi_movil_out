﻿Public Class GeneradorHtml
    Public Function generarHtml(ByVal lista As List(Of Registro))
        Dim html As String

        html = "   <html>" &
                "       <img src='http://test.agenciab12.com/b12/imagenes/b12-logo2.jpg' style='max-width: 100%;'><br/>" &
                "	    <table border='1' bordercolor='navy' cellspacing='0' border-collapse: collapse; text-align: left;>" &
                "		    <thead>" &
                "               <tr> " &
                "                   <th colspan='14' align='center' style='background-color:#FACC2E;height:25px;font-family: Tahoma,Arial,Helvetica,sans-serif,Verdana;font-size:18px;'><b>IZZI MOVIL OUT</b></th>" &
                "		        </tr>" &
                "		        <tr style='font-family: Tahoma,Arial,Helvetica,sans-serif,Verdana;font-size:12px;'> " &
                "			        <th style='background-color:silver;height: 54px; width: 79px' align='center'>&nbsp;&nbsp;Hora&nbsp;&nbsp;<br/>&nbsp;</th>" &
                "			        <th style='background-color:silver;height: 54px; width: 87px' align='center'>Lead<br/>tocados</th>" &
                "			        <th style='background-color:silver;height: 54px; width: 90px' align='center'>Llamadas<br/>contactadas</th>" &
                "			        <th style='background-color:silver;height: 54px; width: 90px' align='center'>Porcent.(%)<br/>Contactacion</th>" &
                "			        <th style='background-color:silver;height: 54px; width: 90px' align='center'>Ventas<br/>móvil</th>" &
                "			        <th style='background-color:silver;height: 54px; width: 90px' align='center'>Ventas<br/>móvil por<br/>hora</th>" &
                "			        <th style='background-color:silver;height: 54px; width: 90px' align='center'>Conversión<br/>general</th>" &
                "		        </tr>" &
                "		    </thead>" &
                "           <tbody>"

        For Each registro As Registro In lista
            If registro.bandera = False Then
                html &= "   <tr style='font-family:Tahoma,Arial,Helvetica,sans-serif,Verdana;font-size:12px;'>" &
                        "	    <td style='background-color:silver;color:black;font-weight:bold;'>" & IIf(registro.hora = 0,
                                                                                                          "FH",
                                                                                                          String.Format("{0:00} h.", registro.hora)) & "</td>" &
                        "	    <td>&nbsp;</td>" &
                        "	    <td>&nbsp;</td>" &
                        "	    <td>&nbsp;</td>" &
                        "	    <td>&nbsp;</td>" &
                        "	    <td>&nbsp;</td>" &
                        "	    <td>&nbsp;</td>" &
                        "   </tr>"
            Else
                html &= "   <tr style='" & clase(registro.hora) & "'>" &
                        "	    <td style='background-color:silver;color:black;font-weight:bold;'>" & IIf(registro.hora = 0,
                                                                                                          "FH",
                                                                                                          String.Format("{0:00} h.", registro.hora)) & "</td>" &
                        "	    <td style='text-align:center'>" & String.Format("{0}", registro.leadsTocados) & "</td>" &
                        "	    <td style='text-align:center'>" & String.Format("{0}", registro.llamadasContactadas) & "</td>" &
                        "	    <td style='text-align:center'>" & String.Format("{0:P1}", registro.porcentajeContactacion) & "</td>" &
                        "	    <td style='text-align:center'>" & String.Format("{0}", registro.ventasMovil) & "</td>" &
                        "	    <td style='text-align:center'>" & String.Format("{0}", registro.ventasMovilHora) & "</td>" &
                        "	    <td style='text-align:center'>" & String.Format("{0}", registro.conversionGeneral) & "</td>" &
                        "   </tr>"
            End If
        Next

        html &= "</tbody></table></div></body></html>"

        Return html
    End Function

    Private Function clase(ByVal hora As Integer) As String
        Return IIf(((hora \ 2) = (hora / 2)) And hora <> 0,
                   "background:#0000;font-family:Tahoma,Arial,Helvetica,sans-serif,Verdana;font-size:12px;",
                   "font-family:Tahoma,Arial,Helvetica,sans-serif,Verdana;font-size:12px;")
    End Function
End Class
