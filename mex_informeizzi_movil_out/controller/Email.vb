﻿Imports System.Net.Mail

Public Class Email
    Private emailDao As EmailDao = New EmailDao()

    Public Sub enviarCorreoInforme(ByVal mensaje As String)
        Try
            Dim correo As MailMessage = New MailMessage()
            correo.From = New MailAddress("soportemx@agenciab12.com", "Soporte Mexico")
            Dim HoraReal As Integer = Hora.horaReal

            Dim cuentas As New List(Of String)

            'If (HoraReal = Hora.horaFinal) Then
            '    cuentas = emailDao.getCuentas("general")
            'Else
            '    cuentas = emailDao.getCuentas("detalle")
            'End If

            'For Each cuenta As String In cuentas
            '    correo.To.Add(New MailAddress(cuenta))
            'Next

            correo.To.Add("desarrollo_mexico@agenciab12.com")
            correo.To.Add("lesanchez@agenciab12.com")
            'correo.To.Add("aclara@agenciab12.com")

            Dim vistaHtml As AlternateView = AlternateView.CreateAlternateViewFromString(mensaje, Nothing, "text/html")
            correo.Subject = "Pruebas informe de ventas/llamadas Izzi Móvil Out " & Hora.horaMexico.Hour & ":00"
            correo.IsBodyHtml = True
            correo.AlternateViews.Add(vistaHtml)
            correo.Priority = System.Net.Mail.MailPriority.Normal
            enviar(correo)
        Catch ex As Exception
            Logger.escribeLog("Error al construir correo de informe " & Hora.horaMexico.Hour & ":00" & vbCrLf & ex.ToString)
            Throw ex
        End Try
    End Sub

    Public Sub enviarCorreoError(ByVal fichero As String, ByVal tipoError As String)
        Try
            Dim correo As New System.Net.Mail.MailMessage
            correo.IsBodyHtml = False
            correo.From = New MailAddress("soportemx@agenciab12.com", "Soporte Mexico")
            Dim lscuentas As New List(Of String)

            'If tipoError = "Exception" Then
            '    lscuentas = emailDao.getCuentas("Error")
            'Else
            '    lscuentas = emailDao.getCuentas("soporte")
            'End If

            'For Each cuenta As String In lscuentas
            '    correo.To.Add(New MailAddress(cuenta))
            'Next

            correo.To.Add("desarrollo_mexico@agenciab12.com")
            correo.To.Add("lesanchez@agenciab12.com")
            'correo.To.Add("aclara@agenciab12.com")


            Dim archivo As New System.Net.Mail.Attachment(fichero)
            correo.Attachments.Add(archivo)
            correo.Subject = "Error informe por hora Izzi Móvil Out " & Now.Date
            correo.IsBodyHtml = True
            correo.Body = ""
            correo.Priority = System.Net.Mail.MailPriority.Normal
            enviar(correo)
        Catch ex As Exception
            Logger.escribeLog("Error al construir correo de error:" & vbCrLf & ex.ToString)
            Throw ex
        End Try
    End Sub

    Private Sub enviar(ByVal correo As MailMessage)
        Try
            Dim smtp As New SmtpClient
            smtp.Credentials =
            New System.Net.NetworkCredential("soportemx@agenciab12.com", "Sup7854mex")
            smtp.Port = 587
            smtp.Host = "smtp.gmail.com"
            smtp.EnableSsl = True
            smtp.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
            smtp.Timeout = 300000
            smtp.ServicePoint.MaxIdleTime = 3000
            smtp.Send(correo)
        Catch ex As Exception
            Logger.escribeLog("Error al enviar correo " & vbCrLf & ex.ToString)
            Throw ex
        End Try
    End Sub
End Class
