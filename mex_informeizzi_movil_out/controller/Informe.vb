﻿Public Class Informe
    Private informeDao As InformeDao = New InformeDao()

    Private hr, leadsFH, llamadasFH As Integer
    Private leads, llamadas, ventas, acumuladoLeads, acumuladoLlamadas, acumuladoVentas As Integer
    Private porcentajeContactacion, conversionGeneral As Double

    Public Function getListaRegistros() As List(Of Registro)
        Dim listaRegistros As List(Of Registro) = New List(Of Registro)
        Try
            'informeDao.abrirConexiones(True)
            llamadasFH = informeDao.getDatosIzziMovilOut("LlamadasFH", 0)
            leadsFH = informeDao.getDatosIzziMovilOut("LeadsFH", 0)
            For item As Integer = Hora.horaInicial To Hora.horaFinal
                If item <= Hora.horaReal Then
                    hr = item - 1

                    If item = Hora.horaInicial Then
                        listaRegistros.Add(New Registro(0, LeadsFH, llamadasFH, 0, 0, 0, 0))
                        acumuladoLeads += leadsFH
                        acumuladoLlamadas += llamadasFH
                    End If

                    leads = informeDao.getDatosIzziMovilOut("Leads", hr)
                    acumuladoLeads += leads

                    llamadas = informeDao.getDatosIzziMovilOut("Llamadas", hr)
                    acumuladoLlamadas += llamadas

                    ventas = informeDao.getVentas(hr)
                    acumuladoVentas += ventas

                    porcentajeContactacion = IIf(acumuladoLeads < 1, 0, (acumuladoLlamadas / acumuladoLeads))
                    conversionGeneral = IIf(leads < 1, 0, CDbl(ventas) / CDbl(leads))


                    listaRegistros.Add(New Registro(item, acumuladoLeads, acumuladoLlamadas, porcentajeContactacion,
                                                    acumuladoVentas, ventas, conversionGeneral))

                Else
                    listaRegistros.Add(New Registro(item))
                End If
            Next
        Catch ex As Exception
            Logger.escribeLog("Error al crear lista de registros" & vbCrLf & ex.ToString)
            Throw ex
        End Try
        Return listaRegistros
    End Function
End Class
