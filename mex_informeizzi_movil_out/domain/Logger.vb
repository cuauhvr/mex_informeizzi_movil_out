﻿Imports System.IO
Public Class Logger

    Private Shared sw As TextWriter = Nothing
    Private Const urlBase As String = "C:\AGENCIAB12\LOGS\REPORTES\InformeHoraIzziMovilOut\"
    'Private Const urlBase As String = "D:\Users\tec08lh1\Documents\pruebas\LOGS\REPORTES\InformeHoraFunerarias\"
    Public Shared Pathtxt As String

    Public Shared Sub crearArchivoLog()
        If Not Directory.Exists(urlBase) Then
            My.Computer.FileSystem.CreateDirectory(urlBase)
        End If
        Pathtxt = (urlBase & "\log_InformeIzziMovilOut.txt")
        If File.Exists(Pathtxt) Then
            File.Delete(Pathtxt)
        End If
    End Sub

    Public Shared Sub escribeLog(ByVal mensaje As String, Optional ByVal instancia As String = "InformeIzziMovilOut")
        Try
            sw = TextWriter.Synchronized(New StreamWriter(Pathtxt, True))
            SyncLock sw
                sw.WriteLine(vbCrLf & vbCrLf & Date.Now & " --- " & mensaje)
                sw.Flush()
                sw.Close()
            End SyncLock
        Catch ex As Exception
            sw.Close()
            escribeLog("Error al escribir en log: " & ex.ToString & " ------ " & mensaje & " -------- " & instancia, "WRITER")
        End Try
    End Sub
End Class
