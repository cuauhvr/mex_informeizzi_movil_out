﻿Public Class Registro
    Public bandera As Boolean = False
    Public hora As String
    Public leadsTocados As Integer
    Public llamadasContactadas As Integer
    Public porcentajeContactacion As Double
    Public ventasMovil As Integer
    Public ventasMovilHora As Integer
    Public conversionGeneral As Integer

    Public Sub New(hora As String)
        Me.hora = hora
    End Sub

    Public Sub New(hora As String, leadsTocados As Integer, llamadasContactadas As Integer, porcentajeContactacion As Double,
                   ventasMovil As Integer, ventasMovilHora As Integer, conversionGeneral As Integer)
        Me.New(hora)
        Me.bandera = True
        Me.leadsTocados = leadsTocados
        Me.llamadasContactadas = llamadasContactadas
        Me.porcentajeContactacion = porcentajeContactacion
        Me.ventasMovil = ventasMovil
        Me.ventasMovilHora = ventasMovilHora
        Me.conversionGeneral = conversionGeneral
    End Sub
End Class
