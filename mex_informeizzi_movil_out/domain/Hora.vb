﻿Public Class Hora
    Public Shared horaMexico As DateTime
    Public Shared horaLocal As DateTime
    Public Shared horaUniversal As DateTime
    Public Shared horaReal As Integer

    Public Shared horaInicial As Integer = 7
    Public Shared horaFinal As Integer = 24

    Public Shared Sub actualizaHoras()
        horaLocal = Now
        horaUniversal = System.TimeZoneInfo.ConvertTimeToUtc(Now)
        horaMexico = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time (Mexico)"))
        horaReal = IIf(horaMexico.Hour = 0, 24, horaMexico.Hour)
    End Sub
End Class
